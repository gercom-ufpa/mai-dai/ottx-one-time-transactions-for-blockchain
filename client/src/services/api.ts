/* eslint-disable no-undef */
import axios from "axios";

export default () => {
  return axios.create({
    headers: {
      "Access-Control-Allow-Origin": import.meta.env.VITE_WALLET_SERVER_HOST,
      "Access-Control-Allow-Methods": "GET,PUT,POST,PATCH,DELETE",
    },
    baseURL: import.meta.env.VITE_WALLET_SERVER_HOST + "/",
  });
};
