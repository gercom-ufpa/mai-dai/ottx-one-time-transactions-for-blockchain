import crypto from "crypto";
import keypair from "keypair";
// Hash function H

function hash(input) {
  return crypto.createHash("sha256").update(input).digest("hex");
}

// Signature algorithm Ssk
function sign(privateKey, message) {
  const signer = crypto.createSign("RSA-SHA256");
  signer.update(message);
  return signer.sign(privateKey, "hex");
}

// Key generation algorithm G
export async function generateKeys() {
  console.time("generateKeys");
  const res = keypair();
  console.timeEnd("generateKeys");
  return { privateKey: res.private, publicKey: res.public };
}

export async function generateInitialOTP(id, password, privateKey, publicKey) {
  console.time("generateInitialOTP1");
  let index = 0;
  let OTP = await hash(JSON.stringify({ id, index, password, privateKey }));
  let nextOTPHash = await hash(OTP);
  let message = { id, publicKey, nextOTPHash };
  let signedMessage = await sign(privateKey, JSON.stringify(message));

  console.timeEnd("generateInitialOTP1");
  let data = { id, publicKey, nextOTPHash, signedMessage };

  let userData = {
    id,
    password,
    otp: { id, OTP, nextOTPHash, index, signedMessage },
    privateKey,
    publicKey,
  };
  return { userData, data };
}

export async function generateOTP(
  id,
  password,
  privateKey,
  publicKey,
  index,
  transactionData
) {
  console.time("generateOTP");

  let nextOTP = await hash(JSON.stringify({ id, index, password, privateKey }));
  let nextOTPHash = await hash(nextOTP);
  let newTxData = { id: transactionData.id, data: transactionData.data };
  let message = { id, publicKey, nextOTPHash, transactionData: newTxData };
  let signedMessage = await sign(privateKey, JSON.stringify(message));

  let OTP = await hash(
    JSON.stringify({ id, index: index - 1, password, privateKey })
  );
  console.timeEnd("generateOTP");
  let otp = { id, OTP, nextOTPHash, index, signedMessage };
  return otp;
}
