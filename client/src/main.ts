/* eslint-disable vue/no-reserved-component-names */
/* eslint-disable vue/multi-word-component-names */
import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import Textarea from "primevue/textarea";
import router from "./router";
import PrimeVue from "primevue/config";
import FileUpload from "primevue/fileupload";
import Password from "primevue/password";
import InputText from "primevue/inputtext";
import Divider from "primevue/divider";
import Button from "primevue/button";
import Message from "primevue/message";
import InputNumber from "primevue/inputnumber";
import ProgressSpinner from "primevue/progressspinner";
import BlockUI from "primevue/blockui";
import Card from "primevue/card";

import "./assets/main.css";

const app = createApp(App);

const pinia = createPinia();
app.use(pinia);
app.use(router);
app.use(PrimeVue);
app.component("FileUpload", FileUpload);
app.component("Password", Password);
app.component("InputText", InputText);
app.component("Divider", Divider);
app.component("Button", Button);
app.component("Message", Message);
app.component("InputNumber", InputNumber);
app.component("ProgressSpinner", ProgressSpinner);
app.component("Card", Card);
app.component("Textarea", Textarea);
app.component("BlockUI", BlockUI);

app.mount("#app");
