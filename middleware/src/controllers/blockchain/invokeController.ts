/*
 * SPDX-License-Identifier: Apache-2.0
 */

import { Gateway, Wallets } from "fabric-network";
import * as path from "path";
import * as fs from "fs";
import { v4 as uuidv4 } from "uuid";
import * as yaml from "js-yaml";

const ccpPath = path.resolve(__dirname, "..", "..", "connection.yaml");
const ccp: any = yaml.load(fs.readFileSync(ccpPath, "utf8"));
const configPath = path.join(__dirname, "..", "..", "./config.json");
const configJSON = fs.readFileSync(configPath, "utf8");
const config = JSON.parse(configJSON);
const walletPath = path.join(__dirname, "..", "..", "wallet");

const register = async (data: any) => {
  try {
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const adminIdentity = await wallet.get("admin");
    if (!adminIdentity) {
      console.log("Run the enrollAdmin.ts application before retrying");
      return {
        status: 500,
        data: {
          message:
            'An identity for the admin user "admin" does not exist in the wallet',
        },
      };
    }

    const gateway = new Gateway();
    await gateway.connect(ccp, {
      wallet,
      identity: "admin",
      discovery: config.gatewayDiscovery,
    });

    const network = await gateway.getNetwork(data.channel);
    const chain = network.getContract(data.chain);

    let args: any = JSON.stringify(data);

    let result: any = await chain.submitTransaction("Invoke", "register", args);
    console.log(JSON.parse(result));

    await gateway.disconnect();
    return {
      status: JSON.parse(result).status,
      data: {
        message: JSON.parse(result).message
          ? JSON.parse(result).message.message
          : "submited",
      },
    };
  } catch (error: any) {
    console.log(error);

    return { status: 500, data: { message: error.toString() } };
  }
};

const authenticate = async (data: any) => {
  try {
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const adminIdentity = await wallet.get("admin");
    if (!adminIdentity) {
      console.log("Run the enrollAdmin.ts application before retrying");
      return {
        status: 500,
        data: {
          message:
            'An identity for the admin user "admin" does not exist in the wallet',
        },
      };
    }

    const gateway = new Gateway();
    await gateway.connect(ccp, {
      wallet,
      identity: "admin",
      discovery: config.gatewayDiscovery,
    });

    const network = await gateway.getNetwork(data.channel);
    const chain = network.getContract(data.chain);

    let args: any = JSON.stringify(data);

    let result: any = await chain.submitTransaction(
      "Invoke",
      "authenticate",
      args
    );
    console.log(JSON.parse(result));

    await gateway.disconnect();
    return {
      status: JSON.parse(result).status,
      data: {
        message: JSON.parse(result).message
          ? JSON.parse(result).message.message
          : "submited",
      },
    };
  } catch (error: any) {
    console.log(error);

    return { status: 500, data: { message: error.toString() } };
  }
};

const logs = async (data: any) => {
  try {
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const adminIdentity = await wallet.get("admin");
    if (!adminIdentity) {
      console.log("Run the enrollAdmin.ts application before retrying");
      return {
        status: 500,
        data: {
          message:
            'An identity for the admin user "admin" does not exist in the wallet',
        },
      };
    }

    const gateway = new Gateway();
    await gateway.connect(ccp, {
      wallet,
      identity: "admin",
      discovery: config.gatewayDiscovery,
    });

    const network = await gateway.getNetwork(data.channel);

    const chain = network.getContract(data.chain);
    let args: any = JSON.stringify(data);
    let queryResult: any = await chain.evaluateTransaction(
      "Invoke",
      "logs",
      args
    );
    console.log(queryResult);
    console.log("queryResult");

    await gateway.disconnect();
    if (!JSON.parse(queryResult).payload) {
      return {
        status: JSON.parse(queryResult).status,
        data: { message: JSON.parse(queryResult).message.message },
      };
    } else {
      const result = JSON.parse(
        Buffer.from(JSON.parse(queryResult).payload).toString()
      );
      if (result[result.length - 1].fetched_records_count < 1) {
        return {
          status: 404,
          data: { message: "Nothing found" },
        };
      }
      if (data.query.id) {
        return {
          status: 200,
          data: result[0].Record,
        };
      }
      if (result[result.length - 1].fetched_records_count > 0) {
        let newResult = {
          data: [],
          pagination: result[result.length - 1],
        };
        result.pop();
        newResult.data = result.map(function (item: any) {
          return item.Record;
        });
        return {
          status: 200,
          data: newResult,
        };
      }
      return {
        status: 200,
        data: result,
      };
    }
  } catch (error: any) {
    console.error(`Failed to submit transaction: ${error}`);

    return { status: 500, data: { message: error.toString() } };
  }
};

export default {
  register,
  authenticate,
  logs,
};
