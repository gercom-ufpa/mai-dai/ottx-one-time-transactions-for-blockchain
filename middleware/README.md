## PREREQUISITE

- Create a config.json file at project root based on one of the files at templates/configurations
- Create a connection.yaml file at project root based on one of the files at templates/connections
- Create the admin.id wallet

## LOCAL

docker build -t blockchain-authenticator-middleware .

docker run -d -p 3000:3000 --name blockchain-authenticator-middleware blockchain-authenticator-middleware


### Local

Start fabric

Change connection.yaml 

Change config.json
