"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const authentication_1 = __importDefault(require("./routes/authentication"));
const dotenv = __importStar(require("dotenv"));
dotenv.config({ path: __dirname + "/.env" });
const cors_1 = __importDefault(require("cors"));
const router = (0, express_1.default)();
/** Logging */
router.use((0, morgan_1.default)("dev"));
router.use((0, cors_1.default)());
/** Parse the request */
router.use(express_1.default.urlencoded({ extended: false }));
/** Takes care of JSON data */
router.use(express_1.default.json());
/** RULES OF OUR API */
router.use((req, res, next) => {
    // set the CORS policy
    res.header("Access-Control-Allow-Origin", "*");
    // set the CORS headers
    res.header("Access-Control-Allow-Headers", "origin, X-Requested-With,Content-Type,Accept, Authorization");
    // set the CORS method headers
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "GET PUT PATCH DELETE POST");
        return res.status(200).json({});
    }
    next();
});
/** Routes */
router.use("/", authentication_1.default);
router.get("/", (req, res) => {
    res.json({
        message: "Welcome to Agrochains Middleware TS.",
    });
});
/** Error handling */
router.use((req, res, next) => {
    const error = new Error("Path not found");
    return res.status(404).json({
        message: error.message,
    });
});
/** Server */
const httpServer = http_1.default.createServer(router);
const SERVER_PORT = (_a = process.env.SERVER_PORT) !== null && _a !== void 0 ? _a : 3001;
httpServer.listen(SERVER_PORT, () => console.log(`The server is running on port ${SERVER_PORT}`));
//# sourceMappingURL=server.js.map